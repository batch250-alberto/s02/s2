package com.zuitt.activitys2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class PrimeNumber {
    public static void main(String args[]){

//        int[] primeNumber = new int[5];
//
//        primeNumber[0] = 2;
//        primeNumber[1] = 3;
//        primeNumber[2] = 5;
//        primeNumber[3] = 7;
//        primeNumber[4] = 11;
//
//        System.out.println("The first prime number is: " + primeNumber[0]);
        System.out.println("Input a number: ");

        int[] primeNumber = {2, 3, 5, 7, 11};
        Scanner input = new Scanner(System.in);
        int newNumber = input.nextInt();

        switch(newNumber) {
            case 1:
                System.out.println("The first prime number is: " + primeNumber[newNumber-1]);
                break;
            case 2:
                System.out.println("The second prime number is: " + primeNumber[newNumber-1]);
                break;
            case 3:
                System.out.println("The third prime number is: " + primeNumber[newNumber-1]);
                break;
            case 4:
                System.out.println("The fourth prime number is: " + primeNumber[newNumber-1]);
                break;
            case 5:
                System.out.println("The fifth prime number is: " + primeNumber[newNumber-1]);
                break;
            default:
                System.out.println("Please choose a number between 1-5!");
                break;
        }


        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
