package com.zuitt.activitys2;

import java.util.Scanner;

public class LeapYear {
    public static void main(String args[]) {

        Scanner numberScanner = new Scanner(System.in);

        System.out.println("Enter a year: ");
        int leapYear = numberScanner.nextInt();

        if(leapYear % 4 == 0 && leapYear % 100 !=0 || leapYear % 400 == 0){
            System.out.println(leapYear + " is a leap year");
        }
        else{
            System.out.println(leapYear + " is not a leap year");
        }
    }
}
